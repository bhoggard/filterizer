class EventsController < ApplicationController
  layout 'admin'
  before_action :authenticate!
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  autocomplete :venue, :name

  # GET /events
  def index
    recent = (Time.zone.now.to_date - 10.days).to_s
    @events = Event.joins(:venue).where("end_date >= ?", recent).includes(:venue).order("end_date DESC")
  end

  # GET /events/1
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
    @event.opening_start_time = '18:00'
    @event.opening_end_time = '20:00'
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to @event, notice: 'Event was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      redirect_to @event, notice: 'Event was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /events/1
  def destroy
    @event.destroy
    redirect_to events_url, notice: 'Event was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def event_params
    params.require(:event).permit(:title, :venue_id, :start_date, :end_date, :opening_date, :opening_start_time, :opening_end_time, :website)
  end
end
