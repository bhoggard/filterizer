module ApplicationHelper
  def body_class
    [controller_name, action_name].join('-')
  end

  def opening_datetime(event)
    dt = "#{event.opening_date.strftime('%A, %B %e')}, #{event.opening_start_time.strftime('%l:%M')}-#{event.opening_end_time.strftime('%l:%M %p')}"
    dt.gsub!(/:00/, '')
    dt.gsub!(/^ /, '')
    dt.gsub(/  /, ' ')
    dt.gsub('- ', '-')
  end

  
end
