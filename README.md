Filterizer
==========

Rails application for http://www.filterizer.com running on Heroku.

[![Maintainability](https://api.codeclimate.com/v1/badges/bc6ab9b8e723e25008a8/maintainability)](https://codeclimate.com/github/bhoggard/filterizer/maintainability)

[![Build Status](https://travis-ci.org/bhoggard/filterizer.svg?branch=master)](https://travis-ci.org/bhoggard/filterizer)
