class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :address
      t.string :website
      t.timestamps
    end
    add_reference :venues, :neighborhood, index: true
  end
end
