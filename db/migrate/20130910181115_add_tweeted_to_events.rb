class AddTweetedToEvents < ActiveRecord::Migration
  def change
    add_column :events, :tweeted, :boolean, null: false, default: false
  end
end
