class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.references :venue, index: true
      t.date :start_date
      t.date :end_date
      t.date :opening_date
      t.time :opening_start_time
      t.time :opening_end_time
      t.string :website
      t.timestamps
    end
    add_index :events, :start_date
    add_index :events, :end_date
    add_index :events, :opening_date
  end
end
