class CreateNeighborhoods < ActiveRecord::Migration
  def change
    create_table :neighborhoods do |t|
      t.string :name
    end
    add_index :neighborhoods, :name, unique: true
  end
end
