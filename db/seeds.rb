# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Neighborhood.count == 0
  %w(Chelsea Lower\ East\ Side Bushwick).each do |name|
    Neighborhood.create!(name: name)
  end
end

if !Rails.env.production? && User.count == 0
  User.create!(name: 'admin', password: 'password', password_confirmation: 'password')
end

