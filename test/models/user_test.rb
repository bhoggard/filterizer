require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "password authentcation" do
    u = User.find_by_name('admin') # seeded
    assert u.authenticate('password')
    refute u.authenticate('not my password')
  end
end
