require 'test_helper'

class LoginTest < ActionDispatch::IntegrationTest
  test "events admin behind login wall" do
    get events_path
    assert_redirected_to login_path
    # user from seeds.db
    post_via_redirect login_path, name: 'admin', password: 'password'
    assert_equal 'Logged in!', flash[:notice]
  end

  test "venues admin behind login wall" do
    get venues_path
    assert_redirected_to login_path
  end
end
