namespace :django do
  desc "Import data from Django version"
  task import: :environment do
    conn = PG.connect(dbname: 'filterizer_django')
    Neighborhood.transaction do
      Neighborhood.delete_all
      conn.exec("select * from artcalendar_neighborhood").each do |row|
        hood = Neighborhood.new(row)
        # puts hood.name
        hood.id = row['id']
        hood.save!
      end

      Venue.delete_all
      conn.exec("select * from artcalendar_venue").each do |row|
        venue = Venue.new(row)
        # puts venue.name
        venue.id = row['id']
        venue.save!
      end

      Event.delete_all
      conn.exec("select * from artcalendar_event").each do |row|
        event = Event.new(row)
        # puts event.name
        event.id = row['id']
        event.save!
      end
    end
  end
end
