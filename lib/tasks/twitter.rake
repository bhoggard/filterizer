desc "Send event tweets to Buffer "
task buffer_event_tweets: :environment do
  buff = Buffer::Client.new(Rails.application.secrets.buffer_key)
  Event.opening_today.not_tweeted.limit(10).each do |event|
    data = {
      profile_ids: [ Rails.application.secrets.buffer_profile_id ], 
      text: event.tweet_text
    }
    data[:media] = { link: event.url } if event.url.present?
    buff.create_update(body: data)
    event.tweeted!
  end
end
