desc "Deploy to production"
task deploy: :environment do
  puts 'Pushing to Heroku'
  sh "git push -f heroku"
  puts 'Running migrations'
  sh "heroku run bundle exec rake db:migrate"
  sh "heroku ps:restart"
end
