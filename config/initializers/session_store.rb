# Be sure to restart your server when you modify this file.

Filterizer::Application.config.session_store :cookie_store, key: '_filterizer_session'

Rails.application.config.action_dispatch.cookies_serializer = :hybrid
